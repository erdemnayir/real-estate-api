<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Contact extends Model
{
    use HasFactory, SoftDeletes;

    const CACHE_LIFETIME_ALL = 60 * 60 * 5;

    const CACHE_KEY_CONTACTS = 'contacts';
    const CACHE_KEY_CONTACT_ID = 'contact-id-';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'phone_number',
        'email_address',
    ];
}
