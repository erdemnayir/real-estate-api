<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Appointment extends Model
{
    use HasFactory, SoftDeletes;

    const CACHE_LIFETIME_ALL = 60 * 60 * 5;

    const CACHE_KEY_APPOINTMENTS = 'appointments';
    const CACHE_KEY_APPOINTMENT_ID = 'appointment-id-';

    protected $fillable = [
        'user_id',
        'contact_id',
        'address',
        'lat',
        'long',
        'appointment_at',
        'distance',
        'left_at',
        'arrived_at',
    ];

    public function worker()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id', 'id');
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function getAppointmentAtAttribute($value): string
    {
        return date("d-m-Y h:i", $value);
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function getLeftAtAttribute($value): string
    {
        return date("d-m-Y h:i", $value);
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function getArrivedAtAttribute($value): string
    {
        return date("d-m-Y h:i", $value);
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function getDistanceAttribute($value): string
    {
        return round($value / 1000, 1). ' KM';
    }
}
