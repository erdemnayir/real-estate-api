<?php

namespace App\Repositories\Eloquent;

use App\Models\Appointment;
use App\Repositories\Contracts\IAppointment;

class AppointmentRepository extends BaseRepository implements IAppointment
{
    /**
     * @return string
     */
    public function model(): string
    {
        return Appointment::class;
    }
}
