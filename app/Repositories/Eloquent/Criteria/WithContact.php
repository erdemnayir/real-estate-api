<?php

namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\Criteria\ICriterion;

class WithContact implements ICriterion
{
    public function apply($model)
    {
        return $model->with('contact:id,first_name,last_name,phone_number,email_address');
    }
}
