<?php

namespace App\Repositories\Eloquent\Criteria;

use App\Repositories\Criteria\ICriterion;

class WithUser implements ICriterion
{
    public function apply($model)
    {
        return $model->with('worker:id,name');
    }
}
