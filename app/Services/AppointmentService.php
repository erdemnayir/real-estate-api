<?php

namespace App\Services;

use App\Models\Appointment;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use App\Repositories\Contracts\IAppointment;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Eloquent\Criteria\IsLive;
use App\Repositories\Eloquent\Criteria\WithUser;
use App\Repositories\Eloquent\Criteria\WithContact;
use App\Repositories\Eloquent\Criteria\LatestFirst;

class AppointmentService
{
    /**
     * @var IAppointment
     */
    protected IAppointment $appointments;

    public function __construct(IAppointment $appointments)
    {
        $this->appointments = $appointments;
    }

    /**
     * @param bool $useCache
     *
     * @return Collection|null
     */
    public function getAllAppointments(bool $useCache = true): Collection|null
    {
        try {
            if ($useCache && !is_null(Cache::get(Appointment::CACHE_KEY_APPOINTMENTS))) {
                return Cache::get(Appointment::CACHE_KEY_APPOINTMENTS);
            }

            $appointments = $this->appointments->withCriteria([
                new LatestFirst(),
                new IsLive(),
                new WithUser(),
                new WithContact(),
            ])->all();

            if ($useCache && !$appointments->isEmpty()) {
                Cache::put(Appointment::CACHE_KEY_APPOINTMENTS, $appointments, Appointment::CACHE_LIFETIME_ALL);
            }

            return $appointments;
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'class' => 'AppointmentService',
                'function' => 'getAllAppointments',
            ]);
        }

        return null;
    }

    /**
     * @param int  $id
     * @param bool $useCache
     *
     * @return Appointment|null
     */
    public function getAppointmentById(int $id, bool $useCache = true): Appointment|null
    {
        try {
            $key = sprintf(Appointment::CACHE_KEY_APPOINTMENT_ID.'%d', $id);

            if ($useCache && !is_null(Cache::get($key))) {
                return Cache::get($key);
            }

            $appointment = $this->appointments->withCriteria([
                new IsLive(),
                new WithUser(),
                new WithContact(),
            ])->find($id);

            if ($useCache && ($appointment instanceof Appointment)) {
                Cache::put($key, $appointment,Appointment::CACHE_LIFETIME_ALL);
            }

            return $appointment;
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'id' => $id,
                'class' => 'AppointmentService',
                'function' => 'getAppointmentById',
            ]);
        }

        return null;
    }

    /**
     * @param array $data
     *
     * @return Appointment|null
     */
    public function createAppointment(array $data): Appointment|null
    {
        try {
            $lat = $data['latlong']['lat'];
            $long = $data['latlong']['long'];
            $appointmentAt = $data['appointment_at'];

            $response = GlobalService::getDistanceWithGoogleAPI($lat, $long);

            if (null === $response) {
                return null;
            }

            $distance = $response['distance']['value'];
            $duration = $response['duration']['value'];

            $leftAt = strtotime($appointmentAt) - $duration;
            $arrivedAt = strtotime($appointmentAt) + $duration + 3600;

            $data['appointment_at'] = strtotime($appointmentAt);
            $data['distance'] = $distance;
            $data['left_at'] = $leftAt;
            $data['arrived_at'] = $arrivedAt;
            $data['lat'] = $lat;
            $data['long'] = $long;

            unset($data['latlong']);

            return $this->appointments->create($data);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'data' => $data,
                'class' => 'AppointmentService',
                'function' => 'createAppointment',
            ]);
        }

        return null;
    }

    /**
     * @param int   $id
     * @param array $data
     *
     * @return Appointment|null
     */
    public function updateAppointment(int $id, array $data): Appointment|null
    {
        try {
            $appointment = $this->getAppointmentById($id);

            if (!($appointment instanceof Appointment)) {
                return null;
            }

            if (!empty($data['appointment_at'])) {
                $appointmentAt = strtotime($data['appointment_at']);
            } else {
                $appointmentAt =  strtotime($appointment->appointment_at);
            }

            if (!empty($data['latlong'])) {
                $lat = $data['latlong']['lat'];
                $long = $data['latlong']['long'];
            } else {
                $lat = $appointment->lat;
                $long = $appointment->long;
            }

            $response = GlobalService::getDistanceWithGoogleAPI($lat, $long);

            if (null === $response) {
                return null;
            }

            $distance = $response['distance']['value'];
            $duration = $response['duration']['value'];

            $leftAt = $appointmentAt - $duration;
            $arrivedAt = $appointmentAt + $duration + 3600;

            $data['appointment_at'] = $appointmentAt;
            $data['distance'] = $distance;
            $data['left_at'] = $leftAt;
            $data['arrived_at'] = $arrivedAt;
            $data['lat'] = $lat;
            $data['long'] = $long;

            unset($data['latlong']);

            return $this->appointments->update($id, $data);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'id' => $id,
                'data' => $data,
                'class' => 'AppointmentService',
                'function' => 'updateAppointment',
            ]);
        }

        return null;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function deleteAppointment(int $id): bool
    {
        try {
            return $this->appointments->delete($id);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'id' => $id,
                'class' => 'AppointmentService',
                'function' => 'deleteAppointment',
            ]);
        }

        return false;
    }
}
