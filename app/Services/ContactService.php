<?php

namespace App\Services;

use App\Models\Contact;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use App\Repositories\Contracts\IContact;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Eloquent\Criteria\IsLive;
use App\Repositories\Eloquent\Criteria\LatestFirst;

class ContactService
{
    /**
     * @var IContact
     */
    protected IContact $contacts;

    public function __construct(IContact $contacts)
    {
        $this->contacts = $contacts;
    }

    /**
     * @param bool $useCache
     *
     * @return Collection|null
     */
    public function getAllContacts(bool $useCache = true): Collection|null
    {
        try {
            if ($useCache && !is_null(Cache::get(Contact::CACHE_KEY_CONTACTS))) {
                return Cache::get(Contact::CACHE_KEY_CONTACTS);
            }

            $contacts = $this->contacts->withCriteria([
                new LatestFirst(),
                new IsLive(),
            ])->all();

            if ($useCache && !$contacts->isEmpty()) {
                Cache::put(Contact::CACHE_KEY_CONTACTS, $contacts, Contact::CACHE_LIFETIME_ALL);
            }

            return $contacts;
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'class' => 'ContactService',
                'function' => 'getAllContacts',
            ]);
        }

        return null;
    }

    /**
     * @param int  $id
     * @param bool $useCache
     *
     * @return Contact|null
     */
    public function getContactById(int $id, bool $useCache = true): Contact|null
    {
        try {
            $key = sprintf(Contact::CACHE_KEY_CONTACT_ID.'%d', $id);

            if ($useCache && !is_null(Cache::get($key))) {
                return Cache::get($key);
            }

            $contact = $this->contacts->find($id);

            if ($useCache && ($contact instanceof Contact)) {
                Cache::put($key, $contact,Contact::CACHE_LIFETIME_ALL);
            }

            return $contact;
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'id' => $id,
                'class' => 'ContactService',
                'function' => 'getContactById',
            ]);
        }

        return null;
    }

    /**
     * @param array $data
     *
     * @return Contact|null
     */
    public function createContact(array $data): Contact|null
    {
        try {
            return $this->contacts->create($data);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'data' => $data,
                'class' => 'ContactService',
                'function' => 'createContact',
            ]);
        }

        return null;
    }

    /**
     * @param int   $id
     * @param array $data
     *
     * @return Contact|null
     */
    public function updateContact(int $id, array $data): Contact|null
    {
        try {
            return $this->contacts->update($id, $data);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'id' => $id,
                'data' => $data,
                'class' => 'ContactService',
                'function' => 'updateContact',
            ]);
        }

        return null;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function deleteContact(int $id): bool
    {
        try {
            return $this->contacts->delete($id);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'id' => $id,
                'class' => 'ContactService',
                'function' => 'deleteContact',
            ]);
        }

        return false;
    }
}
