<?php

namespace App\Services;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class GlobalService
{
    const ICEBERG_ESTATE_POST_CODE = 'cm27pj';
    const ICEBERG_ESTATE_LAT = 51.729157;
    const ICEBERG_ESTATE_LONG = 0.478027;
    const GOOGLE_API_KEY = 'AIzaSyDxit9R3r9dWqxUcIiOZ36bVQbyG28xTBU';
    const BASE_GOOGLE_API_DISTANCE_URL = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.self::ICEBERG_ESTATE_LAT.','.self::ICEBERG_ESTATE_LONG;
    const BASE_POST_CODE_URL = 'api.postcodes.io/postcodes/';

    /**
     * @param string $postCode
     *
     * @return array|null
     */
    public static function getLatLongByPostCode(string $postCode): array|null
    {
        $url = self::BASE_POST_CODE_URL.$postCode;

        $response = Http::get($url);

        if (Response::HTTP_OK === $response->status()) {
            $result = $response->json('result');

            $country = $result['country'];

            if ('England' !== $country) {
                return null;
            }

            return [
              'lat' => $result['latitude'],
              'long' => $result['longitude'],
            ];
        }

        return null;
    }

    /**
     * @param float $lat
     * @param float $long
     *
     * @return array|null
     */
    public static function getDistanceWithGoogleAPI(float $lat, float $long): array|null
    {
        $url = self::BASE_GOOGLE_API_DISTANCE_URL.'&destinations='.$lat.','.$long.'&mode=driving&key='.self::GOOGLE_API_KEY;

        $response = Http::get($url);

        $rows = $response->json('rows');

        $data = [];

        foreach ($rows as $key => $rowItem) {
            foreach ($rowItem as $item) {
                if ('OK' === $item[0]['status']) {
                    $data = [
                      'distance' => $item[0]['distance'],
                      'duration' => $item[0]['duration'],
                    ];
                }
            }
        }

        if (empty($data)) {
            return null;
        }

        return $data;
    }
}
