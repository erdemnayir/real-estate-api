<?php

namespace App\Observers;

use App\Models\Contact;
use Illuminate\Support\Facades\Cache;

class ContactObserver
{
    /**
     * @param Contact $contact
     *
     * @return void
     */
    public function creating(Contact $contact)
    {
        $contact->created_at = new \DateTime();
    }

    /**
     * @param Contact $contact
     *
     * @return void
     */
    public function created(Contact $contact)
    {
        Cache::forget(Contact::CACHE_KEY_CONTACTS);
    }

    /**
     * @param Contact $contact
     *
     * @return void
     */
    public function updating(Contact $contact)
    {
        $contact->updated_at = new \DateTime();
    }

    /**
     * @param Contact $contact
     *
     * @return void
     */
    public function updated(Contact $contact)
    {
        $key = sprintf(Contact::CACHE_KEY_CONTACT_ID.'%d', $contact->id);

        Cache::forget($key);
        Cache::forget(Contact::CACHE_KEY_CONTACTS);
    }

    /**
     * @param Contact $contact
     *
     * @return void
     */
    public function deleted(Contact $contact)
    {
        $key = sprintf(Contact::CACHE_KEY_CONTACT_ID.'%d', $contact->id);

        Cache::forget($key);
        Cache::forget(Contact::CACHE_KEY_CONTACTS);
    }
}
