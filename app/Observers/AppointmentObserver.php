<?php

namespace App\Observers;

use App\Models\Appointment;
use Illuminate\Support\Facades\Cache;

class AppointmentObserver
{
    /**
     * @param Appointment $appointment
     *
     * @return void
     */
    public function creating(Appointment $appointment)
    {
        $appointment->created_at = new \DateTime();
    }

    /**
     * @param Appointment $appointment
     *
     * @return void
     */
    public function created(Appointment $appointment)
    {
        Cache::forget(Appointment::CACHE_KEY_APPOINTMENTS);
    }

    /**
     * @param Appointment $appointment
     *
     * @return void
     */
    public function updating(Appointment $appointment)
    {
        $appointment->updated_at = new \DateTime();
    }

    /**
     * @param Appointment $appointment
     *
     * @return void
     */
    public function updated(Appointment $appointment)
    {
        $key = sprintf($appointment::CACHE_KEY_APPOINTMENT_ID.'%d', $appointment->id);

        Cache::forget($key);
        Cache::forget(Appointment::CACHE_KEY_APPOINTMENTS);
    }

    /**
     * @param  Appointment $appointment
     *
     * @return void
     */
    public function deleted(Appointment $appointment)
    {
        $key = sprintf($appointment::CACHE_KEY_APPOINTMENT_ID.'%d', $appointment->id);

        Cache::forget($key);
        Cache::forget(Appointment::CACHE_KEY_APPOINTMENTS);
    }
}
