<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Contracts\{IAppointment, IContact};
use App\Repositories\Eloquent\{AppointmentRepository, ContactRepository};

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register()
    {}

    /**
     * @return void
     */
    public function boot()
    {
        $this->app->bind(IContact::class, ContactRepository::class);
        $this->app->bind(IAppointment::class, AppointmentRepository::class);
    }
}
