<?php

namespace App\Http\Controllers\api;

use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Services\GlobalService;
use Illuminate\Http\JsonResponse;
use App\Services\AppointmentService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class AppointmentController extends Controller
{
    /**
     * @param AppointmentService $appointmentService
     *
     * @return JsonResponse
     */
    public function index(AppointmentService $appointmentService): JsonResponse
    {
        $appointments = $appointmentService->getAllAppointments();

        if (null === $appointments) {
            return response()->json([
                'error' => 'An error occurred while listing appointments',
            ], 500);
        }

        return response()->json([
            'appointments' => $appointments,
        ], Response::HTTP_OK);
    }

    /**
     * @param int                $id
     * @param AppointmentService $appointmentService
     *
     * @return JsonResponse
     */
    public function getAppointment(int $id, AppointmentService $appointmentService): JsonResponse
    {
        $appointment = $appointmentService->getAppointmentById($id);

        if (!($appointment instanceof Appointment)) {
            return response()->json([
                'error' => sprintf('There is no appointment with id %d', $id),
            ], 500);
        }

        return response()->json([
            'appointment' => $appointment,
        ], Response::HTTP_OK);
    }

    /**
     * @param Request            $request
     * @param AppointmentService $appointmentService
     *
     * @return JsonResponse
     */
    public function store(Request $request, AppointmentService $appointmentService): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|int|exists:users,id',
            'contact_id' => 'required|int|exists:contacts,id,deleted_at,NULL',
            'address' => 'required|string',
            'appointment_at' => 'required|date_format:Y-m-d H:i',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        $appointmentDate = date('d-m-Y H:i', strtotime($request->appointment_at));
        $now = (date('d-m-Y H:i'));

        if ($now > $appointmentDate) {
            return response()->json([
                'error' => 'Choose an appointment date later than today',
            ], 500);
        }

        $latLong = GlobalService::getLatLongByPostCode($request->address);

        if (null === $latLong) {
            return response()->json([
                'error' => 'Please check post code, address could not be found or it is not in England',
            ], 500);
        }

        $data = $request->all();
        $data['latlong'] = $latLong;
        $data['appointment_at'] = $appointmentDate;

        $appointment = $appointmentService->createAppointment($data);

        if (!($appointment instanceof Appointment)) {
            return response()->json([
                'error' => 'An error occurred while creating appointment',
            ], 500);
        }

        return response()->json([
            'appointment' => $appointment,
        ], Response::HTTP_CREATED);
    }

    /**
     * @param int                $id
     * @param Request            $request
     * @param AppointmentService $appointmentService
     *
     * @return JsonResponse
     */
    public function update(int $id, Request $request, AppointmentService $appointmentService): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'int|exists:users,id',
            'contact_id' => 'int|exists:contacts,id,deleted_at,NULL',
            'address' => 'string',
            'appointment_at' => 'date_format:Y-m-d H:i',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        $data = $request->all();

        if ($request->appointment_at ?? false) {
            $appointmentDate = date('d-m-Y H:i', strtotime($request->appointment_at));
            $now = (date('d-m-Y H:i'));

            if ($now > $appointmentDate) {
                return response()->json([
                    'error' => 'Choose an appointment date later than today',
                ], 500);
            }

            $data['appointment_at'] = $appointmentDate;
        }

        if ($request->address ?? false) {
            $latLong = GlobalService::getLatLongByPostCode($request->address);

            if (null === $latLong) {
                return response()->json([
                    'error' => 'Please check post code, address could not be found or it is not in England',
                ], 500);
            }

            $data['latlong'] = $latLong;
        }

        $appointment = $appointmentService->updateAppointment($id, $data);

        if (!($appointment instanceof Appointment)) {
            return response()->json([
                'error' => 'An error occurred while updating appointment',
            ], 500);
        }

        return response()->json([
            'appointment' => $appointment,
        ], Response::HTTP_OK);
    }

    /**
     * @param int                $id
     * @param AppointmentService $appointmentService
     *
     * @return JsonResponse
     */
    public function destroy(int $id, AppointmentService $appointmentService): JsonResponse
    {
        $isDeleted = $appointmentService->deleteAppointment($id);

        return response()->json([
            'isDeleted' => $isDeleted,
        ], Response::HTTP_OK);
    }
}
