<?php

namespace App\Http\Controllers\api;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Services\ContactService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller
{
    /**
     * @param ContactService $contactService
     *
     * @return JsonResponse
     */
    public function index(ContactService $contactService): JsonResponse
    {
        $contacts = $contactService->getAllContacts();

        if (null === $contacts) {
            return response()->json([
                    'error' => 'An error occurred while listing contacts',
                ], 500);
        }

        return response()->json([
            'contacts' => $contacts,
        ], Response::HTTP_OK);
    }

    /**
     * @param int            $id
     * @param ContactService $contactService
     *
     * @return JsonResponse
     */
    public function getContact(int $id, ContactService $contactService): JsonResponse
    {
        $contact = $contactService->getContactById($id);

        if (!($contact instanceof Contact)) {
            return response()->json([
                'error' => sprintf('There is no contact with id %d', $id),
            ], 500);
        }

        return response()->json([
            'contact' => $contact,
        ], Response::HTTP_OK);
    }

    /**
     * @param Request        $request
     * @param ContactService $contactService
     *
     * @return JsonResponse
     */
    public function store(Request $request, ContactService $contactService): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|min:3|max:50',
            'last_name' => 'required|string|min:3|max:50',
            'phone_number' => 'required|string|max:50',
            'email_address' => 'required|string|email|max:100|unique:contacts',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        $data = $request->all();

        $contact = $contactService->createContact($data);

        if (!($contact instanceof Contact)) {
            return response()->json([
                'error' => 'An error occurred while creating contact',
            ], 500);
        }

        return response()->json([
            'contact' => $contact,
        ], Response::HTTP_CREATED);
    }

    /**
     * @param int            $id
     * @param Request        $request
     * @param ContactService $contactService
     *
     * @return JsonResponse
     */
    public function update(int $id, Request $request, ContactService $contactService): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'string|min:3|max:50',
            'last_name' => 'string|min:3|max:50',
            'phone_number' => 'string|max:50',
            'email_address' => 'string|email|max:100|unique:contacts,email_address,'.$id,
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        $data = $request->all();

        $contact = $contactService->updateContact($id, $data);

        if (!($contact instanceof Contact)) {
            return response()->json([
                'error' => 'An error occurred while updating contact',
            ], 500);
        }

        return response()->json([
            'contact' => $contact,
        ], Response::HTTP_OK);
    }

    /**
     * @param int            $id
     * @param ContactService $contactService
     *
     * @return JsonResponse
     */
    public function destroy(int $id, ContactService $contactService): JsonResponse
    {
        $isDeleted = $contactService->deleteContact($id);

        return response()->json([
            'isDeleted' => $isDeleted,
        ], Response::HTTP_OK);
    }
}
