<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\AuthController;
use App\Http\Controllers\api\ContactController;
use App\Http\Controllers\api\AppointmentController;

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::group(['middleware' => 'token', 'prefix' => 'auth'], function () {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::get('me', [AuthController::class, 'profile']);
});

Route::group(['middleware' => 'token', 'prefix' => 'contacts'], function () {
    Route::post('', [ContactController::class, 'store']);
    Route::get('', [ContactController::class, 'index']);
    Route::put('/{id}', [ContactController::class, 'update']);
    Route::get('/{id}', [ContactController::class, 'getContact']);
    Route::delete('/{id}', [ContactController::class, 'destroy']);
});

Route::group(['middleware' => 'token', 'prefix' => 'appointments'], function () {
    Route::post('', [AppointmentController::class, 'store']);
    Route::get('', [AppointmentController::class, 'index']);
    Route::put('/{id}', [AppointmentController::class, 'update']);
    Route::get('/{id}', [AppointmentController::class, 'getAppointment']);
    Route::delete('/{id}', [AppointmentController::class, 'destroy']);
});
