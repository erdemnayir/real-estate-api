<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     *
     */
    function test_login_success(): void
    {
        $this->withExceptionHandling();

        $payload = [
            'email' => 'fazilenesnayir@gmail.com',
            'password' => '123456',
        ];

        $headers = [
            'Accept' => 'application/json',
        ];

        $response = $this->post('/api/login', $payload, $headers);
        $response->assertStatus(200);
    }

    /**
     *
     */
    function test_register_success(): void
    {
        $this->withExceptionHandling();

        $payload = [
            'name' => 'Fazıl Enes Nayir',
            'email' => 'fazilenesnayir@gmail.com',
            'password' => Hash::make('123456'),
        ];

        $headers = [
            'Accept' => 'application/json',
        ];

        $response = $this->post('/api/register', $payload, $headers);

        $response->assertStatus(200);
    }
}
