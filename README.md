# Real Estate Restful API

## Creating database

```bash
php artisan migrate
```

## Run API
```bash
php artisan serve
```

## Login to App
```bash
/api/login [POST]

{
"email": "johndoe@gmail.com",
"password": "123456"
}
```

## Register to App
```bash
/api/register [POST]

{
    "name": "John Doe",
    "email": "johndoe@gmail.com",
    "password": "123456"
}
```

## Profile
```bash
/api/auth/me [GET]
```

## Logout from App
```bash
/api/auth/logout [POST]
```

## Create Contact
```bash
/api/contacts [POST]

{
    "first_name": "Albert",
    "last_name": "Einstein",
    "phone_number": "+44 7911 000000",
    "email_address": "alberteinstein@gmail.com"
}
```

## Get Contacts List
```bash
/api/contacts [GET]
```

## Get Single Contact
```bash
/api/contacts/1 [GET]
```

## Update Contact
```bash
/api/contacts/1 [PUT]

{
    "first_name": "Albert",
    "last_name": "Einstein",
    "phone_number": "+44 7911 000000",
    "email_address": "alberteinstein@gmail.com"
}
```

## Delete Contact
```bash
/api/contacts/1 [DELETE]
```

## Create Appointment
```bash
/api/appointments [POST]

{
    "user_id": 1,
    "contact_id": 3,
    "address": "CM11DD",
    "appointment_at": "2021-12-17 11:00"
}
```

## Get Appointments List
```bash
/api/appointments [GET]
```

## Get Single Appointment
```bash
/api/appointments/1 [GET]
```

## Update Appointment
```bash
/api/appointments/1 [PUT]

{
    "user_id": 1,
    "contact_id": 3,
    "address": "CM11DD",
    "appointment_at": "2021-12-17 11:00"
}
```

## Delete Appointment
```bash
/api/appointments/1 [DELETE]
```
